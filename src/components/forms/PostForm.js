import  React, {Component}  from 'react'
import axios from 'axios'

const BASE_URL='https://jsonplaceholder.typicode.com'

class PostForm extends Component {


    state ={
        title: '',
        body: '',
        userId: '112',
        isSubmitted:false,
        error: false
    }
    changeHandler = (event) => {
            this.setState({
                [event.target.name]:event.target.value
            })
    }


    submitHandler= event=>{
        event.preventDefault();
        axios.post(`${BASE_URL}/posts`,{
            title:this.state.title,
            userId:this.state.userId,
            body:this.state.body
        })
            .then(res =>{
                this.setState({
                    isSubmitted:true,
                    error:false
                });
                console.log(res)
            })
            .catch(error =>{
                this.setState({
                    isSubmitted:false,
                    error:true
                })
            })

    }

    render() {
        return (
            <form onSubmit={this.submitHandler}>
                <h1 className="text-center">Personal Information Form</h1>
                <div className="form-group">
                    <label htmlFor="title">Title</label>
                    <input
                        type="text"
                        name="title"
                        className="form-control"
                        id="title"
                        placeholder="Enter Your Title"
                        value={this.state.title}
                        onChange={this.changeHandler}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="text">Body</label>
                    <textarea
                        type="text"
                        name="body"
                        className="form-control"
                        id="body"
                        placeholder="Enter Your Text Here"
                        value={this.state.body}
                        onChange={this.changeHandler}
                    />
                </div>


                <button type="submit" className="btn btn-success">Submit</button>
                {this.state.isSubmitted && <p>Form Successfully Submitted</p>}
                {this.state.error && <p>Something Wrong</p>}
            </form>



        )
    }


}

export default PostForm