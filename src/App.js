import React, {Component} from 'react';
import './App.css';
import axios from 'axios';
import PostForm from './components/forms/PostForm'



class App extends Component {

    state= {
        posts:[]
    }

    componentDidMount(){
        axios.get('https://jsonplaceholder.typicode.com/posts')
            .then(response=>{
                this.setState({
                    posts:response.data
                })
            })
    }
    render(){
       let {posts}=this.state

        if (posts.length    === 0){
            return(
                <div className="text-center">
                    <div className="spinner-border" role="status">
                        <span className="sr-only">Loading...</span>
                    </div>
                </div>
            );
        }
        else {
            return(
                <div className="App">
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-8 offset-sm-2">
                                <PostForm/>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
    }
}


export default App;
